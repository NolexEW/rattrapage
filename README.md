# CI/CD Gitlab
 
Enoncé du devoir maison "rattrapage" pour les cours Docker et Gitlab CI/CD.
Ce devoir ne s'adresse qu'à celles et ceux qui le souhaitent !
Date de rendu : 28/08/2022 à 23h !
 
## Préambule
 
1. Si vous n'en avez pas déjà un, créer un compte sur Gitlab.com.
2. Vous devez créer un projet qui sera un des deux rendus de ce devoir maison
3. Vous devez m'ajouter en tant que 'Maintainer' à votre projet (@tsaquet)
4. Vous devez paramétrer un runner et l'associer a votre projet
5. Vous devez créer un fichier .gitlab-ci.yml qui permettra de paramétrer tout ce qui est demandé par la suite.
 
Le second rendu est la VM sur laquelle vous aurez tout installé grâce à votre CI/CD Gitlab exposée sur Internet en suivant les consignes plus bas.
 
**Pour toute question, posez là ici, ça peut servir à tout le monde !**
 
## Objectif 1 : Créer une pipeline de build d'image Docker d'un outil Open Source SaaS avec Gitlab
 
* Ajouter un fichier Dockerfile au projet qui permet d'installer le projet open source en mode Saas qui respecte les contraintes suivantes :
  * Un client web avec une page de connexion
  * Une base de donnée type MySQL (MariaDB) ou PostgreSQL
* Si vous n'avez pas d'idée, utilisez [Mattermost](https://mattermost.com/) qui répond à ces contraintes.
* Ajouter l'étape de build de l'image à votre pipeline et stocker le résultat dans la container registry de Gitlab.
 
## Objectif 2 : Ajouter le fichier docker-compose qui permet d'utiliser l'image stockée
 
* Le fichier docker-compose doit utiliser l'image buildée sur Gitlab.
* Il doit permettre de se connecter à une base de données qui est lancée dans un conteneur séparé.
 
Dans les deux cas, attention à la gestion des volumes.
 
## Objectif 3 : Ajouter un conteneur Traefik
 
* Le conteneur doit exposer le tableau de bord de l'application
 
## Objectif 4 : Rendre le logiciel choisi accessible derrière Traefik
 
* Le service du logiciel choisi ne doit pas être exposé en direct, il doit l'être à travers Traefik.
 
## Objectif 5 : Déployer la CI en ligne sur une VM
 
* A chaque fois que le pipeline Gitlab est déclenché, le logiciel choisi doit être déployé sur une VM (GCP / Azure / AWS, au choix).
* Si vous n'avez pas accès à une VM, contactez-moi sur Teams (et uniquement sur Teams), j'ai des comptes Azure pour ceux qui en ont besoin. Ce n'est pas nécessaire pour démarrer le projet, il y a plein d'autres choses à faire, je les distribuerai au fur et à mesure dans les jours qui arrivent.
 
## Objectif 6 : Faire du HTTPS
 
* Pour ça il vous faut un nom de domaine. Si vous n'en avez pas sous la main, vous pouvez utiliser [NetLibre](https://netlib.re) pour en créer un. Si vous ne savez pas configurer un nom de domaine pour qu'il pointe vers une machine à vous, il y a un [tuto ici](https://linuxintosh.wordpress.com/2016/03/28/acces-au-serveur-web-owncloud-a-partir-de-son-nom-de-domaine/).
* Une fois que vous avez votre nom de domaine, il n'y a "plus qu'à" configurer Traefik pour utiliser [Let's Encrypt](https://letsencrypt.org/fr/) !
 
## Conseil généraux
 
* Il y a beaucoup de ressources en ligne pour faire tout ce qui est demandé ici, la plupart du travail consiste à trouver les bonnes ressources et à les agencer entre elles. Attention de prendre des ressources officielles et pas des trucs qui viennent d'obscures sites web, ça vous a joué des tours dans l'année
